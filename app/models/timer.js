import DS from 'ember-data';

export default DS.Model.extend({
	//interval: DS.attr('number'),
	completedSessions: DS.attr('number'),
	name: DS.attr('string')
}).reopenClass({
	FIXTURES: [
		{
			id: 1,
			//interval: 25,
			completedSessions: 0,
			name: "Math"
		}
	]
});